from django.contrib import admin

# глава 1.13
from bboard.models import Bb, Rubric

# admin.site.register(Bb)

class BbAdmin(admin.ModelAdmin):
    list_display = ('title', 'content', 'price', 'published', 'rubric')
    list_display_links = ('title', 'content')
    search_fields = ('title', 'content',)

admin.site.register(Bb, BbAdmin)
admin.site.register(Rubric)