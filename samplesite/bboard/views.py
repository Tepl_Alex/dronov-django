from django.shortcuts import render
from django.http import HttpResponse

from bboard.models import Bb, Rubric

# глава 1.5
# def index(request):
#     return HttpResponse("Здесь будет выведен список объявлений.")

# # глава 1.10 листинг 1.8
# def index(request):
#     s = "Список объявлений \r\n\r\n\r\n"
#     for bb in Bb.objects.order_by("-published"):
#         s += f"{bb.title} \r\n {bb.content} \r\n\r\n"
#     return HttpResponse(s, content_type='text/plain; charset=utf-8')

# глава 1.12 листинг 1.10
#
# from django.template import loader
#
# def index(request):
#     template = loader.get_template('bboard/index.html')
#     bbs = Bb.objects.order_by('-published')
#     context = {'bbs': bbs}
#
#     return HttpResponse(template.render(context, request))

# глава 1.12 листинг 1.11
#
from django.shortcuts import render

def index(request):
    # bbs = Bb.objects.order_by('-published')
    # глава 1.14
    bbs = Bb.objects.all()
    rubrics = Rubric.objects.all()
    context = {'bbs': bbs, 'rubrics': rubrics}
    return render(request, 'bboard/index.html', context)


def by_rubric(request, rubric_id):
    bbs = Bb.objects.filter(rubric=rubric_id)
    rubrics = Rubric.objects.all()
    current_rubric = Rubric.objects.get(pk=rubric_id)
    context = {
        "bbs": bbs,
        'rubrics': rubrics,
        "current_rubric": current_rubric
    }
    return render(request, 'bboard/by_rubric.html', context)


# глава 2.6 листинг 2.7
from django.views.generic.edit import CreateView

from bboard.forms import BbForm
from django.urls import reverse_lazy


class BbCreateView(CreateView):
    template_name = 'bboard/create.html'
    form_class = BbForm
    success_url = reverse_lazy('index')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['rubrics'] = Rubric.objects.all()
        return context
